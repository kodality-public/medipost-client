package com.kodality.medipost.processor;

import com.kodality.medipost.MedipostConfig;
import com.kodality.medipost.model.message.MessageItem;
import com.kodality.medipost.model.notification.NotificationMessage;
import com.kodality.medipost.model.order.OrderMessage;
import com.kodality.medipost.model.result.ResultMessage;
import java.io.FileInputStream;

import java.io.IOException;
import java.util.Properties;

public class MedipostMessageProcessorExample extends MedipostMessageProcessor {

  private MedipostMessageProcessorExample(MedipostConfig medipostConfig) {
    super(medipostConfig);
  }

  public static void main(String[] args) throws IOException {
    Properties prop = new Properties();

    prop.load(new FileInputStream("config.properties"));
    String password = prop.getProperty("medipost.password");
    String user = prop.getProperty("medipost.user");

    MedipostConfig config =
        MedipostConfig.builder().disableHostnameVerification(true)
            .url("https://veeb.medisoft.ee:7443/Medipost/MedipostServlet")
            .user(user)
            .password(password).build();

    MedipostMessageProcessor processor = new MedipostMessageProcessorExample(config);

    processor.process();

  }

  @Override
  protected NotificationMessage handleOrder(OrderMessage order) {
    System.out.println("handling order message:");
    System.out.println(order);
    return new NotificationMessage();
  }

  @Override
  protected NotificationMessage handleResult(ResultMessage result) {
    System.out.println("handling result message:");
    System.out.println(result);
    return new NotificationMessage();
  }

  @Override
  protected void handleNotification(NotificationMessage notification) {
    System.out.println("handling notification message:");
    System.out.println(notification);
  }

  @Override
  protected void handleError(RuntimeException e, MessageItem messageItem) {
    System.out.println("error");
    System.out.println(e.getMessage());
    System.out.println(messageItem);
  }
}
