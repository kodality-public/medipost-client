package com.kodality.medipost.model

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.kodality.medipost.model.servicelist.ServiceListMessage
import com.kodality.medipost.util.MapperUtil
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path

class ServiceListMessageSpec extends Specification {
  def "test deserialization"() {
    XmlMapper xmlMapper = MapperUtil.getMapper()
    def messageText = Files.readString(Path.of(ServiceListMessage.class.getClassLoader().getResource("TeenusedLOINC.xml").toURI()))
    def message = xmlMapper.readValue(messageText, ServiceListMessage.class)

    expect:
    message.services.performers[0].organization.id.content == '10047646'
    message.services.performers[0].definitionGroups[0].definitionGroupId.content == 'KUG'
    message.services.performers[0].inputParameters[0].sequenceNr.content == 1501
  }
}
