package com.kodality.medipost.model

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.kodality.medipost.model.common.YesNo
import com.kodality.medipost.model.order.OrderMessage
import com.kodality.medipost.util.MapperUtil
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path

class OrderMessageSpec extends Specification {

  def "test deserialization"() {
    XmlMapper xmlMapper = MapperUtil.getMapper()
    def messageText = Files.readString(Path.of(OrderMessage.class.getClassLoader().getResource("TellimusLOINC.xml").toURI()))
    def message = xmlMapper.readValue(messageText, OrderMessage.class)
    expect:
    message.order.organizations?.size() == 2
    message.order.personal?.size() == 3
    message.order.specimens?.size() == 9

    message.order.organizations[1].type == 'TEOSTAJA'
    message.order.organizations[1].id.content == '10047646'
    message.order.specimens[0].customerSpecimenId.content == '3041'
    message.order.inputParameters[1].nameUsedInLab.content == 'Sisend 2'

    message.order.cito == YesNo.YES
  }

}
