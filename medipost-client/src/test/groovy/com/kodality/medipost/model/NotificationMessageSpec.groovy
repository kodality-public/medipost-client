package com.kodality.medipost.model

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.kodality.medipost.model.message.PacketType
import com.kodality.medipost.model.notification.NotificationCode
import com.kodality.medipost.model.notification.NotificationMessage
import com.kodality.medipost.util.MapperUtil
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path

class NotificationMessageSpec extends Specification {

  def "test deserialization"() {
    XmlMapper xmlMapper = MapperUtil.getMapper();
    def messageText = Files.readString(Path.of(NotificationMessage.class.getClassLoader().getResource("Teade.xml").toURI()))
    def message = xmlMapper.readValue(messageText, NotificationMessage.class)
    expect:
    message.header.packet.content == PacketType.NOTIFICATION
    message.notification.code == NotificationCode.OK
    message.notification.oldPacket.oldPacketId.content == 'test.4'
  }

}
