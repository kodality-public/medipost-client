package com.kodality.medipost;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class MedipostConfig {
  private final String user;
  private final String password;
  private final String url;

  /**
   *   in dev/test environment the server host is different from the certificate host, jvm will give hostname verification error
   *   disabling hostname verification solves this, but it also disables it for whole jvm
    */
  private final boolean disableHostnameVerification;
}
