package com.kodality.medipost.client;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MedipostClientException extends RuntimeException {

  private int status;
  private String operation;
  private String url;

  public MedipostClientException() {
  }

  public MedipostClientException(String message) {
    super(message);
  }

  public MedipostClientException(String message, Throwable cause) {
    super(message, cause);
  }

  public MedipostClientException(Throwable cause) {
    super(cause);
  }

  @Override
  public String toString() {
    return String.format("MedipostClientException: {status: %d, operation: %s, url: %s, exceptionMessage: %s}", status, operation, url, getMessage());
  }
}
