package com.kodality.medipost.client;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.kodality.medipost.util.MapperUtil;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.Properties;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class MedipostHttpClient {

  private final HttpClient httpClient;
  private final XmlMapper xmlMapper;

  public MedipostHttpClient() {
    this(false);
  }

  public MedipostHttpClient(boolean disableHostnameVerification) {
    this.httpClient = initHttpClient(disableHostnameVerification);
    xmlMapper = MapperUtil.getMapper();
  }

  public <T> T doGet(String url, Class<T> responseType) {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create(url))
        .header("Content-Type", "application/x-www-form-urlencoded")
        .timeout(Duration.ofMinutes(1))
        .GET()
        .build();
    return doRequest(url, responseType, request);
  }


  public <T> T doPost(String url, MultiPartBodyPublisher publisher, Class<T> responseType) {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create(url))
        .header("Content-Type", "multipart/form-data; boundary=" + publisher.getBoundary())
        .timeout(Duration.ofMinutes(1))
        .POST(publisher.build())
        .build();
    return doRequest(url, responseType, request);
  }

  private <T> T doRequest(String url, Class<T> responseType, HttpRequest request) {
    try {
      HttpResponse<String> resp = this.httpClient.send(request, BodyHandlers.ofString());
      if (resp.statusCode() != 200) {
        throw new MedipostClientException(resp.statusCode(), request.method(), url);
      }
      String body = resp.body();
      log.debug(body);
      if (!responseType.equals(Response.class) && body != null && body.startsWith("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<ANSWER")) {
        throw new MedipostClientException(body);
      }
      return xmlMapper.readValue(body, responseType);
    } catch (IOException | InterruptedException e) {
      throw new MedipostClientException(e);
    }
  }



  private HttpClient initHttpClient(boolean disableHostnameVerification) {
    if (disableHostnameVerification) {
      // this is jvm global, would be better to have some per-client solution
      final Properties props = System.getProperties();
      props.setProperty("jdk.internal.httpclient.disableHostnameVerification", Boolean.TRUE.toString());
    }
    return HttpClient.newBuilder().build();
  }


}
