
package com.kodality.medipost.model.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Notification {

  @JacksonXmlProperty(localName = "TeateTyyp")
  private NotificationType type;
  @JacksonXmlProperty(localName = "TeateKood")
  private NotificationCode code;
  @JacksonXmlProperty(localName = "TeateKommentaar")
  private Field comment;
  @JacksonXmlProperty(localName = "VanaPakett")
  private OldPacket oldPacket;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;



}
