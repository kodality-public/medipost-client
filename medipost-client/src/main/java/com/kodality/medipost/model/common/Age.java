
package com.kodality.medipost.model.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Age {

  @JacksonXmlProperty(localName = "Paevi")
  @JsonInclude
  private IntField days;
  @JacksonXmlProperty(localName = "Kuid")
  @JsonInclude
  private IntField months;
  @JacksonXmlProperty(localName = "Aastaid")
  @JsonInclude
  private IntField years;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;

}
