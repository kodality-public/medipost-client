package com.kodality.medipost.model.servicelist;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.kodality.medipost.model.message.Header;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JacksonXmlRootElement(localName = "Saadetis")
public class ServiceListMessage {
  @JacksonXmlProperty(localName = "Pais")
  private Header header;

  @JacksonXmlProperty(localName = "Teenused")
  private Services services;
}
