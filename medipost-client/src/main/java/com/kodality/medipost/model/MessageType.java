package com.kodality.medipost.model;

import java.util.Arrays;
import lombok.Getter;

@Getter
public enum MessageType {

  Order("Tellimus"),
  Notification("Teade"),
  Result("Vastus"),
  Services("Teenus");

  MessageType(String code) {
    this.code = code;
  }

  private String code;

  public static MessageType fromCode(String code) {
    return Arrays.stream(MessageType.values()).filter(mt -> mt.getCode().equals(code)).findFirst().get();
  }
}
