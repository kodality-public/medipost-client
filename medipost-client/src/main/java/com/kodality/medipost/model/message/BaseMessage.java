package com.kodality.medipost.model.message;

import com.kodality.medipost.model.MessageType;

public interface BaseMessage {
  Header getHeader();
  void setHeader(Header header);

  MessageType getType();
}
