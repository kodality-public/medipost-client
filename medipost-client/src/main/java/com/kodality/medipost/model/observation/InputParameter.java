
package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InputParameter {

  @JacksonXmlProperty(localName = "UuringIdOID")
  private Field observationIdOID;
  @JacksonXmlProperty(localName = "UuringId")
  private Field observationId;
  /**
   * DO NOT USE CAMEL CASE LIKE THIS 'tCode' - IT BREAKS XML SERIALIZER
   */
  @JacksonXmlProperty(localName = "TLyhend")
  private Field tcode;
  @JacksonXmlProperty(localName = "KNimetus")
  private Field usedName;
  @JacksonXmlProperty(localName = "UuringNimi")
  private Field nameUsedInLab;
  @JacksonXmlProperty(localName = "TellijaUuringId")
  private Field customerObservationId;
  @JacksonXmlProperty(localName = "Jarjekord")
  private IntField sequenceNr;
  @JacksonXmlProperty(localName = "Kirjeldus")
  private Field description;
  @JacksonXmlProperty(localName = "AsendatavVastusId")
  private Field replacedResultId;
  @JacksonXmlProperty(localName = "SisendparameetriOlek")
  private Field inputParameterStatus;
  @JacksonXmlProperty(localName = "Mootyhik")
  private Field measurementUnit;
  @JacksonXmlProperty(localName = "SisendparameetriVastus")
  private InputParameterResult inputParameterResult;

  @JacksonXmlProperty(localName = "VastuseTyyp", isAttribute = true)
  private String resultType;
  @JacksonXmlProperty(localName = "Formaat", isAttribute = true)
  private String format;
  @JacksonXmlProperty(localName = "VastuseKoodistikuNimi", isAttribute = true)
  private String codeSystemName;
  @JacksonXmlProperty(localName = "VastuseKoodistikuOID", isAttribute = true)
  private String codeSystemOID;
  @JacksonXmlProperty(localName = "URL", isAttribute = true)
  private String codeSystemUrl;

  @JacksonXmlProperty(localName = "VoimalikVaartus")
  private List<PossibleInputValue> possibleInputValues;

  public interface InputParameterResultType {
    String NUMBER = "ARV";
    String FREE_TEXT = "VABATEKST";
    String ENCODED = "KODEERITUD";
    String TIMESTAMP = "AJAHETK";
  }
}
