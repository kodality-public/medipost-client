package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.common.YesNo;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InputParamRelation {
  @JacksonXmlProperty(localName = "SisendparameetriJnr", isAttribute = true)
  private Integer inputParamSeqNr;
  @JacksonXmlProperty(localName = "Kohustuslik", isAttribute = true)
  private YesNo required;
  @JacksonXmlProperty(localName = "Sisestaja", isAttribute = true)
  private EntererType enterer;
  @JacksonXmlProperty(localName = "VoimalikVaartusId")
  private List<Field> possibleValueIds;

}
