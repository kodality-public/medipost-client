package com.kodality.medipost.model.common;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PriceItem {
  @JacksonXmlProperty(localName = "HkKood")
  protected Field hifCode;
  @JacksonXmlProperty(localName = "HkKoodiKordaja")
  protected Field hifCodeMultiplier;
  @JacksonXmlProperty(localName = "Koefitsient")
  protected Field coefficient;
  @JacksonXmlProperty(localName = "Hind")
  protected Field price;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  protected String error;
}
