package com.kodality.medipost.model.servicelist;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Services {
  @JacksonXmlProperty(localName = "Teostaja")
  private List<Performer> performers;
}
