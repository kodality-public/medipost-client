
package com.kodality.medipost.model.common;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResultValue {

  @JacksonXmlText
  private String content;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;
  @JacksonXmlProperty(localName = "tingimus", isAttribute = true)
  private String condition;
  @JacksonXmlProperty(localName = "lugeja", isAttribute = true)
  private String quotient;


}
