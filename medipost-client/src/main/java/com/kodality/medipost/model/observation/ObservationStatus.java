package com.kodality.medipost.model.observation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum ObservationStatus {
  @XmlEnumValue("1") QUEUED,
  @XmlEnumValue("2") ON_HOLD,
  @XmlEnumValue("3") PROCESSING,
  @XmlEnumValue("4") COMPLETED,
  @XmlEnumValue("5") REJECTED,
  @XmlEnumValue("6") CANCELLED;
}
