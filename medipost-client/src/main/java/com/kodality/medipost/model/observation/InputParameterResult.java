
package com.kodality.medipost.model.observation;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.DateTimeField;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import com.kodality.medipost.model.common.NormLower;
import com.kodality.medipost.model.common.NormUpper;
import com.kodality.medipost.model.common.ResultValue;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InputParameterResult {

  @JacksonXmlProperty(localName = "VastuseVaartus")
  private ResultValue resultValue;
  @JacksonXmlProperty(localName = "VastuseTolgendus")
  private Field resultInterpretation;
  @JacksonXmlProperty(localName = "VastuseKoodOID")
  private Field resultCodeOID;
  @JacksonXmlProperty(localName = "VastuseKoodOIDNimi")
  private Field resultCodeOIDName;
  @JacksonXmlProperty(localName = "VastuseKood")
  private Field resultCode;
  @JacksonXmlProperty(localName = "VastuseAeg")
  private DateTimeField resultDate;
  @JacksonXmlProperty(localName = "VastuseSisestajaJnr")
  private Field resultAuthorSequenceNr;
  @JacksonXmlProperty(localName = "NormYlem")
  private NormUpper normUpper;
  @JacksonXmlProperty(localName = "NormAlum")
  private NormLower normLower;
  @JacksonXmlProperty(localName = "NormiKommentaar")
  private Field normComment;
  @JacksonXmlProperty(localName = "NormiStaatus")
  private Field normStatus;
  @JacksonXmlProperty(localName = "ProoviJarjenumber")
  private List<IntField> specimenSequenceNumbers;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;

}
