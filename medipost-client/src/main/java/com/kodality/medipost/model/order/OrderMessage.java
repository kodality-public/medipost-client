
package com.kodality.medipost.model.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.kodality.medipost.model.MessageType;
import com.kodality.medipost.model.message.Header;
import com.kodality.medipost.model.message.BaseMessage;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JacksonXmlRootElement(localName = "Saadetis")
public class OrderMessage implements BaseMessage {

  @JacksonXmlProperty(localName = "Pais")
  private Header header;

  @JacksonXmlProperty(localName = "Tellimus")
  private Order order;

  @JsonIgnore
  public MessageType getType() {
    return MessageType.Order;
  }

}
