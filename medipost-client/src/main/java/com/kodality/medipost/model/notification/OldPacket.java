
package com.kodality.medipost.model.notification;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OldPacket {

    @JacksonXmlProperty(localName = "VanaPaketiId")
    private Field oldPacketId;
    @JacksonXmlProperty(localName = "ViganePakett")
    private Field errorPacket;
    @JacksonXmlProperty(localName = "veakood", isAttribute=true)
    private String error;


}
