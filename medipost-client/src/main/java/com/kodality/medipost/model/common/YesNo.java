package com.kodality.medipost.model.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum YesNo {
  @XmlEnumValue("JAH") YES,
  @XmlEnumValue("EI") NO;
}

