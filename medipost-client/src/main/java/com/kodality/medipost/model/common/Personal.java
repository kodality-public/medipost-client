
package com.kodality.medipost.model.common;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.kodality.medipost.model.Field;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Personal {

  @JacksonXmlProperty(localName = "PersonalOID")
  private Field personOID;
  @JacksonXmlProperty(localName = "PersonalKood")
  private Field code;
  @JacksonXmlProperty(localName = "PersonalPerekonnaNimi")
  private Field lastName;
  @JacksonXmlProperty(localName = "PersonalEesNimi")
  private Field firstName;
  @JacksonXmlProperty(localName = "Eriala")
  private Field speciality;
  @JacksonXmlProperty(localName = "Telefon")
  private Field phone;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;
  @JacksonXmlProperty(localName = "tyyp", isAttribute = true)
  private String type;
  @JacksonXmlProperty(localName = "jarjenumber", isAttribute = true)
  private Integer sequenceNumber;

  public interface PersonType {
    String CUSTOMER = "TELLIJA";
    String PERFORMER = "TEOSTAJA";
    String ENTERER = "SISESTAJA";
  }

}
