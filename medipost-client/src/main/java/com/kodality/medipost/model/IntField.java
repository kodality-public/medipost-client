package com.kodality.medipost.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IntField {
  @JacksonXmlText
  private Integer content;
  @JacksonXmlProperty(localName = "veakood", isAttribute = true)
  private String error;

  public IntField() {

  }

  public IntField(Integer content) {
    this.content = content;
  }
}
