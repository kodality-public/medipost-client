package com.kodality.medipost.app.mapper.to;

import com.kodality.medipost.app.model.Coding;
import com.kodality.medipost.app.model.MedipostMessage;
import com.kodality.medipost.app.model.MessageType;
import com.kodality.medipost.app.model.order.Price;
import com.kodality.medipost.model.DateField;
import com.kodality.medipost.model.DateTimeField;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import com.kodality.medipost.model.common.Organization;
import com.kodality.medipost.model.common.PriceItem;
import com.kodality.medipost.model.message.Header;
import com.kodality.medipost.model.message.Packet;
import com.kodality.medipost.model.message.Packet.Version;
import com.kodality.medipost.model.message.PacketType;
import com.kodality.medipost.model.observation.Container;
import io.micronaut.core.util.CollectionUtils;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Function;

public class ToMedipostMapper {
  private Packet mapPacket(MessageType type) {
    Packet p = new Packet();
    p.setVersion(Version._20);
    p.setContent(mapMessageType(type));
    return p;
  }

  private PacketType mapMessageType(MessageType type) {
    return switch (type) {
      case order -> PacketType.ORDER;
      case result -> PacketType.ANSWER;
      case service_list -> PacketType.SERVICE_LIST;
      case ack -> PacketType.NOTIFICATION;
    };
  }

  protected List<Field> mkField(List<String> value) {
    return CollectionUtils.isEmpty(value) ? null : value.stream().map(this::mkField).toList();
  }

  protected List<IntField> mkIntField(List<Integer> value) {
    return CollectionUtils.isEmpty(value) ? null : value.stream().map(this::mkField).toList();
  }

  protected IntField mkField(Integer value) {
    return value == null ? null : new IntField(value);
  }

  protected DateTimeField mkField(LocalDateTime value) {
    return value == null ? null : new DateTimeField(value);
  }

  protected Field mkField(String value) {
    return value == null ? null : new Field(value);
  }

  protected DateField mkField(LocalDate value) {
    return value == null ? null : new DateField(value);
  }

  protected Header mapHeader(MedipostMessage app) {
    Header header = new Header();
    header.setSender(mkField(app.getSender()));
    header.setRecipient(mkField(app.getRecipient()));
    header.setPacket(mapPacket(app.getMessageType()));
    header.setSenderMessageId(mkField(app.getMessageId()));
    header.setDate(mkField(app.getMessageTime()));
    header.setEmail(mkField(app.getSenderEmail()));
    return header;
  }

  protected <T,U> List<U> map(List<T> app, Function<T, U> mapper) {
    return CollectionUtils.isEmpty(app) ? null : app.stream().map(mapper).toList();
  }

  protected Organization mapOrganization(com.kodality.medipost.app.model.order.Organization app, String type) {
    Organization org = new Organization();

    org.setId(mkField(app.getRegCode()));
    org.setName(mkField(app.getName()));
    org.setCode(mkField(app.getMedipostCode()));
    org.setSubOrgName(mkField(app.getSubstructureName()));
    org.setPhone(mkField(app.getPhone()));
    org.setMunicipality(mkField(app.getMunicipalityEhakCode()));
    org.setAddress(mkField(app.getAddress()));
    org.setType(type);
    org.setSequenceNumber(app.getSequenceNo());

    return org;
  }

  protected PriceItem mapPrice(Price app) {
    PriceItem i = new PriceItem();

    i.setHifCode(mkField(app.getCode()));
    i.setHifCodeMultiplier(mkField(app.getMultiplier()));
    i.setCoefficient(mkField(app.getCoefficient()));
    i.setPrice(mkField(app.getPrice()));

    return i;
  }

  protected Container mapContainer(Coding app) {
    if (app == null) {
      return null;
    }

    Container c = new Container();

    c.setCodeOID(mkField(app.getSystem()));
    c.setCode(mkField(app.getValue()));
    c.setName(mkField(app.getName()));
    c.setDescription(mkField(app.getDescription()));

    return c;
  }
}
