package com.kodality.medipost.app.controller;

import com.kodality.commons.util.JsonUtil;
import com.kodality.medipost.app.client.MedipostClientForApp;
import com.kodality.medipost.app.model.ack.AckMessage;
import com.kodality.medipost.app.model.order.OrderMessage;
import com.kodality.medipost.app.model.order.ResultMessage;
import com.kodality.medipost.app.model.servicelist.ServiceListMessage;
import com.kodality.medipost.client.Response;
import io.micronaut.context.annotation.Parameter;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.exceptions.HttpStatusException;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Controller("/messages")
public class MedipostController {
  private final MedipostClientForApp medipostClient;

  @Get("/private")
  public String getListOfPrivateMessages() {
    return toJson(medipostClient.getPrivateMessageList());
  }

  @Get("/private/orders/{messageId}")
  public String getOrderMessage(@Parameter String messageId) {
    return medipostClient.getOrderMessage(messageId)
        .map(this::toJson)
        .orElseThrow(() -> notFound("Order message for not found for id " + messageId));
  }

  @Post("private/orders")
  public void sendOrderMessage(@Body OrderMessage message) {
    checkResponse(medipostClient.sendOrderMessage(message));
  }

  @Get("/private/results/{messageId}")
  public String getResultMessage(@Parameter String messageId) {
    return medipostClient.getResultMessage(messageId)
        .map(this::toJson)
        .orElseThrow(() -> notFound("Result message for not found for id " + messageId));
  }

  @Post("private/results")
  public void sendResultMessage(@Body ResultMessage message) {
    checkResponse(medipostClient.sendResultMessage(message));
  }

  @Get("/private/acks/{messageId}")
  public String getAckMessage(@Parameter String messageId) {
    return medipostClient.getAckMessage(messageId)
        .map(this::toJson)
        .orElseThrow(() -> notFound("Ack message for not found for id " + messageId));
  }

  @Post("private/acks")
  public void sendAckMessage(@Body AckMessage message) {
    checkResponse(medipostClient.sendAckMessage(message));
  }

  @Delete("private/{messageId}")
  public void deletePrivateMessage(@Parameter String messageId) {
    checkResponse(medipostClient.deletePrivateMessage(messageId));
  }

  private static void checkResponse(Response response) {
    if (!response.isOk()) {
      throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Medipost response: code=" + response.getCode() + ", text=" + response.getText());
    }
  }

  @Get("/service-list")
  public String getListOfServiceListMessages() {
    return toJson(medipostClient.getListOfServiceListMessages());
  }

  @Get("service-list/{messageId}")
  public String getServiceListById(@Parameter String messageId) {
    return medipostClient.getServiceListById(messageId)
        .map(this::toJson)
        .orElseThrow(() -> notFound("Service list message not found for id " + messageId));
  }

  @Get("service-list/latest/{sender}")
  public String getLatestServiceList(@Parameter String sender) {
    return medipostClient.getLatestServiceList(sender)
        .map(this::toJson)
        .orElseThrow(() -> notFound("Latest service list message not found for " + sender));
  }

  @Post("service-list")
  public void sendServiceList(@Body ServiceListMessage message) {
    checkResponse(medipostClient.sendServiceList(message));
  }

  @Delete("service-list/{messageId}")
  public void deleteServiceList(@Parameter String messageId) {
    checkResponse(medipostClient.deletePublicMessage(messageId));
  }

  private String toJson(Object o) {
    return JsonUtil.toJson(o);
  }

  private static HttpStatusException notFound(String message) {
    return new HttpStatusException(HttpStatus.NOT_FOUND, message);
  }
}