package com.kodality.medipost.app.mapper.from;

import com.kodality.medipost.app.model.list.MessageListItem;
import com.kodality.medipost.app.model.MessageType;
import com.kodality.medipost.model.message.MessageItem;
import com.kodality.medipost.model.message.MessageList;
import jakarta.inject.Singleton;
import java.util.List;

@Singleton
public class FromMedipostMessageListMapper {
  public List<MessageListItem> map(MessageList mp) {
    return mp.messages.stream().map(this::mapItem).toList();
  }

  private MessageListItem mapItem(MessageItem mp) {
    return new MessageListItem()
        .setMessageId(mp.getId())
        .setReceived(mp.getDate())
        .setMessageType(mapMessageType(mp.getType()))
        .setSender(mp.getSender());
  }

  private MessageType mapMessageType(String mp) {
    return switch (mp) {
      case "Teade" -> MessageType.ack;
      case "Tellimus" -> MessageType.order;
      case "Vastus" -> MessageType.result;
      case "Teenus" -> MessageType.service_list;
      default -> throw new IllegalArgumentException(mp);
    };
  }
}
