package com.kodality.medipost.app.model.order;

import com.kodality.medipost.app.model.Coding;
import java.util.LinkedList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class LabWorkUnit {
  private String placerOrderId;
  private Patient patient;
  private Organization placerOrganization;
  private Practitioner placerPractitioner;
  private List<Organization> performerOrganizations;
  private List<Practitioner> performerPractitioners;
  private List<Practitioner> dataEntererPractitioners;
  private List<Annotation> notes;
  private List<Coding> referralDiagnoses;
  private List<Specimen> specimens;
  private List<ObservationGroup> observationGroups;

  public LabWorkUnit addNote(String text, AnnotationOrigin origin) {
    if (text == null || text.isBlank()) {
      return this;
    }
    if (notes == null) {
      notes = new LinkedList<>();
    }
    notes.add(new Annotation().setText(text).setOrigin(origin));
    return this;
  }
}