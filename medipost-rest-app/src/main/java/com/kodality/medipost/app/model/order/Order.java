package com.kodality.medipost.app.model.order;

import com.kodality.medipost.app.model.Coding;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Order extends LabWorkUnit {
  private Confidentiality confidentiality;
  private Referral referral;
  private Coding sicknessCaseId;
  private Coding commonId;
  private List<Observation> inputParameters;
  private boolean cito;
}