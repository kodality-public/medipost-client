package com.kodality.medipost.app.mapper.from;

import com.kodality.medipost.app.model.Coding;
import com.kodality.medipost.app.model.MedipostMessage;
import com.kodality.medipost.app.model.MessageType;
import com.kodality.medipost.app.model.order.Organization;
import com.kodality.medipost.app.model.order.Price;
import com.kodality.medipost.model.DateTimeField;
import com.kodality.medipost.model.Field;
import com.kodality.medipost.model.IntField;
import com.kodality.medipost.model.common.PriceItem;
import com.kodality.medipost.model.message.Header;
import com.kodality.medipost.model.message.Packet;
import com.kodality.medipost.model.observation.Container;
import com.kodality.medipost.model.observation.InputParameter;
import com.kodality.medipost.model.observation.ObservationDefElement;
import io.micronaut.core.util.CollectionUtils;
import io.micronaut.core.util.StringUtils;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

public class FromMedipostMapper {
  protected String value(Field field) {
    return field != null && !isEmpty(field.getContent()) ? field.getContent() : null;
  }

  protected LocalDateTime value(DateTimeField field) {
    return field != null ? field.getContent() : null;
  }

  protected <T extends MedipostMessage> T mapHeader(Header mp, T message) {
    message
        .setSender(value(mp.getSender()))
        .setRecipient(value(mp.getRecipient()))
        .setMessageType(mapMessageType(mp.getPacket()))
        .setMessageId(value(mp.getSenderMessageId()))
        .setMessageTime(value(mp.getDate()))
        .setSenderEmail(value(mp.getEmail()));
    return message;
  }

  private MessageType mapMessageType(Packet mp) {
    return switch (mp.getContent()) {
      case SERVICE_LIST -> MessageType.service_list;
      case ORDER -> MessageType.order;
      case ANSWER -> MessageType.result;
      case NOTIFICATION -> MessageType.ack;
    };
  }

  protected Organization mapOrganization(com.kodality.medipost.model.common.Organization mp) {
    return new Organization()
        .setRegCode(value(mp.getId()))
        .setMedipostCode(value(mp.getCode()))
        .setName(value(mp.getName()))
        .setSubstructureName(value(mp.getSubOrgName()))
        .setPhone(value(mp.getPhone()))
        .setMunicipalityEhakCode(value(mp.getMunicipality()))
        .setAddress(value(mp.getAddress()))
        .setSequenceNo(mp.getSequenceNumber());
  }

  protected Integer value(IntField field) {
    return field != null ? field.getContent() : null;
  }

  protected boolean isSet(Field field) {
    return field != null && !isEmpty(field.getContent());
  }

  protected Coding cd(Field system, Field value) {
    return isSet(system) || isSet(value) ? new Coding().setSystem(value(system)).setValue(value(value)) : null;
  }

  protected boolean isEmpty(String s) {
    return StringUtils.isEmpty(s);
  }

  protected boolean isEmpty(Collection<?> collection) {
    return CollectionUtils.isEmpty(collection);
  }

  protected <T,U> List<U> map(List<T> app, Function<T, U> mapper) {
    return CollectionUtils.isEmpty(app) ? null : app.stream().map(mapper).toList();
  }

  protected Coding mapInputParameterCode(InputParameter mp) {
    Coding c = cd(mp.getObservationIdOID(), mp.getObservationId());
    return c == null
        ? null
        : c
        .setName(value(mp.getNameUsedInLab()))
        .setDescription(value(mp.getDescription()))
        .setUsableName(value(mp.getUsedName()))
        .setTAbbreviation(value(mp.getTcode()));
  }

  protected List<String> values(List<Field> mp) {
    return isEmpty(mp) ? null : mp.stream().map(this::value).toList();
  }

  protected Price mapPrice(PriceItem mp) {
    return new Price()
        .setCode(value(mp.getHifCode()))
        .setMultiplier(value(mp.getHifCodeMultiplier()))
        .setCoefficient(value(mp.getCoefficient()))
        .setPrice(value(mp.getPrice()));
  }

  protected Coding mapObservationCode(ObservationDefElement mp) {
    if (mp == null) {
      return null;
    }
    Coding c = cd(mp.getDefinitionIdOID(), mp.getDefinitionId());
    return c == null
        ? null
        : c
        .setName(value(mp.getNameUsedInLab()))
        .setUsableName(value(mp.getUsedName()))
        .setTAbbreviation(value(mp.getTcode()));
  }

  protected Integer intValue(Field field) {
    String value = value(field);
    return isEmpty(value) ? null : Integer.valueOf(value);
  }

  protected Coding mapContainer(Container mp) {
    if (mp == null) {
      return null;
    }
    Coding c = cd(mp.getCodeOID(), mp.getCode());
    return c == null
        ? null
        : c.setName(value(mp.getName())).setDescription(value(mp.getDescription()));
  }
}
