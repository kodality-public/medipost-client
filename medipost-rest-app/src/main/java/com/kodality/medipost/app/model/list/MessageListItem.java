package com.kodality.medipost.app.model.list;

import com.kodality.medipost.app.model.MessageType;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class MessageListItem {
  private String messageId;
  private LocalDateTime received;
  private MessageType messageType;
  private String sender;
}