package com.kodality.medipost.app.mapper.from;

import com.kodality.medipost.app.model.Coding;
import com.kodality.medipost.app.model.order.AnnotationOrigin;
import com.kodality.medipost.app.model.order.Observation;
import com.kodality.medipost.app.model.order.Order;
import com.kodality.medipost.app.model.order.OrderMessage;
import com.kodality.medipost.app.model.order.ReferenceRange;
import com.kodality.medipost.app.model.order.Referral;
import com.kodality.medipost.model.common.Personal.PersonType;
import com.kodality.medipost.model.common.YesNo;
import com.kodality.medipost.model.observation.InputParameter;
import com.kodality.medipost.model.observation.InputParameterResult;
import com.kodality.medipost.model.order.SicknessCase;
import jakarta.inject.Singleton;
import java.util.Collections;

@Singleton
public class FromMedipostOrderMapper extends FromMedipostLabWorkUnitMapper {

  public OrderMessage map(com.kodality.medipost.model.order.OrderMessage mp) {
    return mapHeader(mp.getHeader(), new OrderMessage()).setOrder(mapOrder(mp.getOrder()));
  }

  private Order mapOrder(com.kodality.medipost.model.order.Order mp) {
    Order order = new Order();

    order
        .setConfidentiality(mapConfidentiality(mp.getConfidentiality()))
        .setReferral(mapReferral(mp.getReferral()))
        .setSicknessCaseId(mapSicknessCase(mp.getSicknessCase()))
        .setCommonId(mapCommonId(mp))
        .setInputParameters(map(mp.getInputParameters(), this::mapInputParameter))
        .setCito(mp.getCito() == YesNo.YES)
        .setPlacerOrderId(value(mp.getCustomerOrderId()))
        .setPlacerOrganization(mapPlacerOrganization(mp.getOrganizations()))
        .setPerformerOrganizations(mapPerformerOrganizations(mp.getOrganizations()))
        .setPlacerPractitioner(mapPlacerPractitioner(mp.getPersonal()))
        .setPerformerPractitioners(mapPractitioners(mp.getPersonal(), PersonType.PERFORMER))
        .setDataEntererPractitioners(mapPractitioners(mp.getPersonal(), PersonType.ENTERER))
        .addNote(value(mp.getCustomerComment()), AnnotationOrigin.placer)
        .setReferralDiagnoses(map(mp.getReferralDiagnoses(), this::mapReferralDiagnosis))
        .setPatient(mapPatient(mp.getPatient()))
        .setSpecimens(map(mp.getSpecimens(), this::mapSpecimen))
        .setObservationGroups(map(mp.getObservationGroups(), this::mapObservationGroup));

    return order;
  }

  private Observation mapInputParameter(InputParameter mp) {
    Observation obs = new Observation()
        .setCode(mapInputParameterCode(mp))
        .setPlacerObservationId(value(mp.getCustomerObservationId()))
        .setSequenceNo(value(mp.getSequenceNr()))
        .setReplacedObservationId(value(mp.getReplacedResultId()))
        .setStatus(mapStatus(value(mp.getInputParameterStatus())));

    InputParameterResult mpRes = mp.getInputParameterResult();
    if (mpRes != null) {
        obs
            .setResultUnit(value(mp.getMeasurementUnit()))
            .setCodedResult(mapCodedResult(mpRes))
            .setResult(mapNotCodedResult(mpRes))
            .setVerificationTime(value(mpRes.getResultDate()))
            .setPerformerPractitionerSequenceNo(intValue(mpRes.getResultAuthorSequenceNr()) == null
                ? null : Collections.singletonList(intValue(mpRes.getResultAuthorSequenceNr())))
            .setInterpretation(value(mpRes.getResultInterpretation()))
            .setReferenceRange(mapReferenceRange(mpRes))
            .setSpecimenSequenceNo(intValues(mpRes.getSpecimenSequenceNumbers()));
    }

    return obs;
  }

  private ReferenceRange mapReferenceRange(InputParameterResult mp) {
    return mapReferenceRange(mp.getNormLower(), mp.getNormUpper(), value(mp.getNormComment()), value(mp.getNormStatus()));
  }


  private String mapNotCodedResult(InputParameterResult mp) {
    return cd(mp.getResultCodeOID(), mp.getResultCode()) != null ? null : mapResultValue(mp.getResultValue());
  }

  private Coding mapCodedResult(InputParameterResult mp) {
    Coding c = cd(mp.getResultCodeOID(), mp.getResultCode());
    return c == null ? null : c.setName(mapResultValue(mp.getResultValue()));
  }

  private Coding mapSicknessCase(SicknessCase mp) {
    return mp == null ? null : cd(mp.getSicknessCaseOID(), mp.getSicknessCaseNumber());
  }

  private Coding mapCommonId(com.kodality.medipost.model.order.Order mp) {
    return cd(mp.getCommonIdOID(), mp.getCommonId());
  }

  private Referral mapReferral(com.kodality.medipost.model.order.Referral mp) {
    return mp == null
        ? null
        : new Referral()
        .setIdentifier(cd(mp.getReferralNumberOID(), mp.getReferralNumber()))
        .setType(cd(mp.getReferralTypeOID(), mp.getReferralType()));
  }
}
