package com.kodality.medipost.app.model.servicelist;

import com.kodality.medipost.app.model.Coding;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class SpecimenDefinition {
  private Coding code;
  private String containerProperty;
  private List<Coding> containers;
  private Integer sequenceNo;
  private Boolean bodySiteRequired;
  private List<Coding> bodySites;
}