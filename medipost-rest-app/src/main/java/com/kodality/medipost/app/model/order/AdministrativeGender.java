package com.kodality.medipost.app.model.order;

public enum AdministrativeGender {
  male,
  female,
  unknown
}