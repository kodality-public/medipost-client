package com.kodality.medipost.app.model.order;

public enum ReferenceRangeInterpretation {
  normal,
  warning,
  alert
}