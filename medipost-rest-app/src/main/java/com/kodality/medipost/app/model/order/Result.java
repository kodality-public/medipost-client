package com.kodality.medipost.app.model.order;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Result extends LabWorkUnit {
  private String performerOrderId;
  private ObservationStatus status;
}