package com.kodality.medipost.app.model.order;

import com.kodality.medipost.app.model.Coding;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Confidentiality {
  private Coding forPatient;
  private Coding forPractitioner;
  private Coding forRepresentative;
  private LocalDateTime patientOpenDate;
}