package com.kodality.medipost.app.model.servicelist;

public enum InputParameterEnterer {
  order_placer,
  specimen_collector
}
