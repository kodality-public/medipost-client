package com.kodality.medipost.app.model.order;

public enum ObservationStatus {
  ordered,
  on_hold,
  in_progress,
  aborted,
  completed,
  nullified
}