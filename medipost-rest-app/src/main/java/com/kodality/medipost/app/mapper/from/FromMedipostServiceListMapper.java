package com.kodality.medipost.app.mapper.from;

import com.kodality.medipost.app.model.Coding;
import com.kodality.medipost.app.model.order.ResultType;
import com.kodality.medipost.app.model.servicelist.AdditionalInfoDefinition;
import com.kodality.medipost.app.model.servicelist.InputParameterDefinition;
import com.kodality.medipost.app.model.servicelist.InputParameterEnterer;
import com.kodality.medipost.app.model.servicelist.InputRequirement;
import com.kodality.medipost.app.model.servicelist.ObservationDefinition;
import com.kodality.medipost.app.model.servicelist.ObservationGroupDefinition;
import com.kodality.medipost.app.model.servicelist.ServiceList;
import com.kodality.medipost.app.model.servicelist.ServiceListMessage;
import com.kodality.medipost.app.model.servicelist.SpecimenDefinition;
import com.kodality.medipost.app.model.servicelist.SpecimenGroupDefinition;
import com.kodality.medipost.model.common.YesNo;
import com.kodality.medipost.model.observation.AdditionalInfo;
import com.kodality.medipost.model.observation.BodySite;
import com.kodality.medipost.model.observation.DefinitionSpecimen;
import com.kodality.medipost.model.observation.DefinitionSpecimenGroup;
import com.kodality.medipost.model.observation.EntererType;
import com.kodality.medipost.model.observation.InputParamRelation;
import com.kodality.medipost.model.observation.InputParameter;
import com.kodality.medipost.model.observation.InputParameter.InputParameterResultType;
import com.kodality.medipost.model.observation.ObservationDef;
import com.kodality.medipost.model.observation.ObservationDefGroup;
import com.kodality.medipost.model.servicelist.Performer;
import jakarta.inject.Singleton;

@Singleton
public class FromMedipostServiceListMapper extends FromMedipostMapper {
  public ServiceListMessage map(com.kodality.medipost.model.servicelist.ServiceListMessage mp) {
    return mapHeader(mp.getHeader(), new ServiceListMessage())
        .setServiceLists(map(mp.getServices() == null ? null : mp.getServices().getPerformers(), this::mapServiceList));
  }

  private ServiceList mapServiceList(Performer mp) {
    return new ServiceList()
        .setPerformer(mapOrganization(mp.getOrganization()))
        .setInputParameters(map(mp.getInputParameters(), this::mapInputParameterDefinition))
        .setObservationGroups(map(mp.getDefinitionGroups(), this::mapObservationGroupDefinition));
  }

  private ObservationGroupDefinition mapObservationGroupDefinition(ObservationDefGroup mp) {
    return new ObservationGroupDefinition()
        .setCode(new Coding().setValue(value(mp.getDefinitionGroupId())).setName(value(mp.getDefinitionGroupName())))
        .setAdditionalInfo(map(mp.getAdditionalInfo(), this::mapAdditionalInfoDefinition))
        .setObservations(map(mp.getDefinitions(), this::mapObservationDefinition))
        .setPrices(map(mp.getPrices(), this::mapPrice))
        .setSequenceNo(value(mp.getSequenceNumber()));
  }

  private ObservationDefinition mapObservationDefinition(ObservationDef mp) {
    return new ObservationDefinition()
        .setCode(mapObservationCode(mp.getElement()))
        .setOrderable(YesNo.YES == mp.getOrderable())
        .setResultUnit(mp.getElement() == null ? null : value(mp.getElement().getMeasurementUnit()))
        .setSequenceNo(mp.getElement() == null ? null : intValue(mp.getElement().getSequence()))
        .setInputRequirements(mp.getElement() == null
            ? null : map(mp.getElement().getInputParamRelations(), this::mapInputRequirement))
        .setSpecimenDefinitionSequenceNo(mp.getElement() == null ? null : value(mp.getElement().getSpecimenDefSequenceNr()))
        .setSpecimenGroupDefinitions(map(mp.getDefinitionSpecimenGroups(), this::mapSpecimenGroupDefinition));
  }

  private SpecimenGroupDefinition mapSpecimenGroupDefinition(DefinitionSpecimenGroup mp) {
    return new SpecimenGroupDefinition()
        .setPreferred(YesNo.YES == mp.getPreferred())
        .setSpecimenDefinitions(map(mp.getDefinitionSpecimens(), this::mapSpecimenDefinition));
  }

  private SpecimenDefinition mapSpecimenDefinition(DefinitionSpecimen mp) {
    Coding code = cd(mp.getSpecimenTypeOID(), mp.getSpecimenType());

    return new SpecimenDefinition()
        .setCode(code == null ? null : code.setName(value(mp.getSpecimenName())))
        .setContainerProperty(value(mp.getContainerProperty()))
        .setSequenceNo(value(mp.getSequenceNumber()))
        .setContainers(map(mp.getContainers(), this::mapContainer))
        .setBodySites(mp.getSpecimenBodySite() == null ? null : map(mp.getSpecimenBodySite().getBodySites(), this::mapBodySite))
        .setBodySiteRequired(mp.getSpecimenBodySite() == null ? null : (mp.getSpecimenBodySite().getRequired() == YesNo.YES));
  }

  private Coding mapBodySite(BodySite mp) {
    return new Coding().setSystem(mp.getBodySiteOID()).setValue(mp.getCode()).setName(mp.getName());
  }

  private InputRequirement mapInputRequirement(InputParamRelation mp) {
    return new InputRequirement()
        .setInputParameterDefinitionSequenceNo(mp.getInputParamSeqNr())
        .setRequired(mp.getRequired() == YesNo.YES)
        .setEnterer(mapInputParameterEnterer(mp.getEnterer()))
        .setPossibleResultCodes(values(mp.getPossibleValueIds()));
  }

  private InputParameterEnterer mapInputParameterEnterer(EntererType mp) {
    if (mp == null) {
      return null;
    }

    return switch (mp) {
      case AT_ORDERING -> InputParameterEnterer.order_placer;
      case AT_COLLECTION -> InputParameterEnterer.specimen_collector;
    };
  }

  private AdditionalInfoDefinition mapAdditionalInfoDefinition(AdditionalInfo mp) {
    return new AdditionalInfoDefinition()
        .setCode(new Coding().setValue(value(mp.getId())).setName(value(mp.getName())))
        .setPossibleValues(values(mp.getValue()));
  }

  private InputParameterDefinition mapInputParameterDefinition(InputParameter mp) {
    return new InputParameterDefinition()
        .setCode(mapInputParameterCode(mp))
        .setResultType(mapInputParameterResultType(mp.getResultType()))
        .setResultUnit(value(mp.getMeasurementUnit()))
        .setFormat(mp.getFormat())
        .setCodeSystemOID(mp.getCodeSystemOID())
        .setCodeSystemUrl(mp.getCodeSystemUrl())
        .setCodeSystemName(mp.getCodeSystemName())
        .setPossibleResults(map(mp.getPossibleInputValues(),
            v -> new Coding().setValue(value(v.getId())).setName(value(v.getValue()))))
        .setSequenceNo(value(mp.getSequenceNr()));
  }

  private ResultType mapInputParameterResultType(String resultType) {
    if (resultType == null) {
      return null;
    }

    return switch (resultType) {
      case InputParameterResultType.NUMBER -> ResultType.quantity;
      case InputParameterResultType.FREE_TEXT -> ResultType.narrative;
      case InputParameterResultType.ENCODED -> ResultType.coded;
      case InputParameterResultType.TIMESTAMP -> ResultType.timepoint;
      default -> throw new IllegalArgumentException(resultType);
    };
  }
}
