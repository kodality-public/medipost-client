package com.kodality.medipost.app.model.order;

import com.kodality.medipost.app.model.Coding;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ObservationGroup {
  private Coding code;
  private List<Observation> observations;
  private List<AdditionalInfo> additionalInfo;
  private List<Price> prices;
}