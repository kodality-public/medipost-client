package com.kodality.medipost.app.model.order;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Annotation {
  private String text;
  private boolean placer;
  private AnnotationOrigin origin;
  // Add author and time when needed.
}