package com.kodality.medipost.app.mapper.from;

import com.kodality.medipost.app.model.order.AnnotationOrigin;
import com.kodality.medipost.app.model.order.Result;
import com.kodality.medipost.app.model.order.ResultMessage;
import com.kodality.medipost.model.common.Personal.PersonType;
import jakarta.inject.Singleton;

@Singleton
public class FromMedipostResultMapper extends FromMedipostLabWorkUnitMapper {

  public ResultMessage map(com.kodality.medipost.model.result.ResultMessage mp) {
    return mapHeader(mp.getHeader(), new ResultMessage()).setResult(mapResult(mp.getResult()));
  }

  private Result mapResult(com.kodality.medipost.model.result.Result mp) {
    Result result = new Result();

    result
        .setPerformerOrderId(value(mp.getLabOrderId()))
        .setStatus(mp.getOrderStatus() == null ? null : mapStatus(mp.getOrderStatus().name()))
        .setPlacerOrderId(value(mp.getCustomerOrderId()))
        .setPlacerOrganization(mapPlacerOrganization(mp.getOrganizations()))
        .setPerformerOrganizations(mapPerformerOrganizations(mp.getOrganizations()))
        .setPlacerPractitioner(mapPlacerPractitioner(mp.getPersonal()))
        .setPerformerPractitioners(mapPractitioners(mp.getPersonal(), PersonType.PERFORMER))
        .setDataEntererPractitioners(mapPractitioners(mp.getPersonal(), PersonType.ENTERER))
        .addNote(value(mp.getCustomerComment()), AnnotationOrigin.placer)
        .addNote(value(mp.getLabComment()), AnnotationOrigin.lab)
        .setReferralDiagnoses(map(mp.getReferralDiagnoses(), this::mapReferralDiagnosis))
        .setPatient(mapPatient(mp.getPatient()))
        .setSpecimens(map(mp.getSpecimens(), this::mapSpecimen))
        .setObservationGroups(map(mp.getObservationGroups(), this::mapObservationGroup));

    return result;
  }
}