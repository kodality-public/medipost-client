package com.kodality.medipost.app.model;

public enum MessageType {
  order,
  result,
  service_list,
  ack
}