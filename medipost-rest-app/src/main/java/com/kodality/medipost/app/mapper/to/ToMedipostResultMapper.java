package com.kodality.medipost.app.mapper.to;

import com.kodality.medipost.app.model.order.Annotation;
import com.kodality.medipost.app.model.order.ObservationStatus;
import com.kodality.medipost.model.result.Result;
import com.kodality.medipost.model.result.ResultMessage;
import com.kodality.medipost.model.result.ResultStatus;
import io.micronaut.core.util.CollectionUtils;
import jakarta.inject.Singleton;

@Singleton
public class ToMedipostResultMapper extends ToMedipostLabWorkUnitMapper {

  public ResultMessage map(com.kodality.medipost.app.model.order.ResultMessage app) {
    ResultMessage msg = new ResultMessage();

    msg.setHeader(mapHeader(app));
    msg.setResult(mapResult(app.getResult()));

    return msg;
  }

  private Result mapResult(com.kodality.medipost.app.model.order.Result app) {
    Result result = new Result();

    result.setCustomerOrderId(mkField(app.getPlacerOrderId()));
    result.setOrderStatus(mapStatus(app.getStatus()));
    result.setLabOrderId(mkField(app.getPerformerOrderId()));
    result.setOrganizations(mapOrganizations(app));
    result.setPersonal(mapPractitioners(app));
    result.setCustomerComment(CollectionUtils.isEmpty(app.getNotes())
        ? null : mapNotes(app.getNotes().stream().filter(Annotation::isPlacer).toList()));
    result.setLabComment(CollectionUtils.isEmpty(app.getNotes())
        ? null : mapNotes(app.getNotes().stream().filter(n -> !n.isPlacer()).toList()));
    result.setReferralDiagnoses(map(app.getReferralDiagnoses(), this::mapReferralDiagnosis));
    result.setPatient(mapPatient(app.getPatient()));
    result.setSpecimens(map(app.getSpecimens(), this::mapSpecimen));
    result.setObservationGroups(map(app.getObservationGroups(), this::mapObservationGroup));

    return result;
  }

  private ResultStatus mapStatus(ObservationStatus app) {
    if (app == null) {
      return null;
    }

    return switch (app) {
      case ordered -> ResultStatus.QUEUED;
      case on_hold -> ResultStatus.ON_HOLD;
      case in_progress -> ResultStatus.PROCESSING;
      case aborted -> ResultStatus.REJECTED;
      case completed -> ResultStatus.COMPLETED;
      case nullified -> ResultStatus.CANCELLED;
    };
  }
}
