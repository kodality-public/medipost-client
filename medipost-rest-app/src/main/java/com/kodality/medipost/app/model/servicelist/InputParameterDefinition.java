package com.kodality.medipost.app.model.servicelist;

import com.kodality.medipost.app.model.Coding;
import com.kodality.medipost.app.model.order.ResultType;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InputParameterDefinition {
  private Coding code;
  private ResultType resultType;
  private String resultUnit;
  private String format;
  private String codeSystemName;
  private String codeSystemOID;
  private String codeSystemUrl;
  private List<Coding> possibleResults;
  private Integer sequenceNo;
}