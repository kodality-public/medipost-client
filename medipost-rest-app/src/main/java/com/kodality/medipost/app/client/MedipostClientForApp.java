package com.kodality.medipost.app.client;

import com.kodality.medipost.app.mapper.from.FromMedipostAckMapper;
import com.kodality.medipost.app.mapper.from.FromMedipostMessageListMapper;
import com.kodality.medipost.app.mapper.from.FromMedipostOrderMapper;
import com.kodality.medipost.app.mapper.from.FromMedipostResultMapper;
import com.kodality.medipost.app.mapper.from.FromMedipostServiceListMapper;
import com.kodality.medipost.app.mapper.to.ToMedipostAckMapper;
import com.kodality.medipost.app.mapper.to.ToMedipostOrderMapper;
import com.kodality.medipost.app.mapper.to.ToMedipostResultMapper;
import com.kodality.medipost.app.mapper.to.ToMedipostServiceListMapper;
import com.kodality.medipost.app.model.ack.AckMessage;
import com.kodality.medipost.app.model.list.MessageListItem;
import com.kodality.medipost.app.model.order.OrderMessage;
import com.kodality.medipost.app.model.order.ResultMessage;
import com.kodality.medipost.app.model.servicelist.ServiceListMessage;
import com.kodality.medipost.client.MedipostClient;
import com.kodality.medipost.client.Response;
import com.kodality.medipost.model.notification.NotificationMessage;
import jakarta.inject.Singleton;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;

@Singleton
@RequiredArgsConstructor
public class MedipostClientForApp {
  private final MedipostClient medipostClient;

  private final FromMedipostServiceListMapper fromMedipostServiceListMapper;
  private final FromMedipostAckMapper fromMedipostAckMapper;
  private final FromMedipostOrderMapper fromMedipostOrderMapper;
  private final FromMedipostResultMapper fromMedipostResultMapper;
  private final FromMedipostMessageListMapper fromMedipostMessageListMapper;

  private final ToMedipostAckMapper toMedipostAckMapper;
  private final ToMedipostOrderMapper toMedipostOrderMapper;
  private final ToMedipostResultMapper toMedipostResultMapper;
  private final ToMedipostServiceListMapper toMedipostServiceListMapper;

  public List<MessageListItem> getPrivateMessageList() {
    return fromMedipostMessageListMapper.map(medipostClient.getPrivateMessageList());
  }

  public Optional<OrderMessage> getOrderMessage(String messageId) {
    com.kodality.medipost.model.order.OrderMessage msg = medipostClient.getOrder(messageId);
    return Optional.ofNullable(msg == null || msg.getOrder() == null ? null : fromMedipostOrderMapper.map(msg));
  }

  public Response sendOrderMessage(OrderMessage message) {
    return medipostClient.sendOrder(toMedipostOrderMapper.map(message), message.getRecipient());
  }

  public Optional<ResultMessage> getResultMessage(String messageId) {
    com.kodality.medipost.model.result.ResultMessage msg = medipostClient.getResult(messageId);
    return Optional.ofNullable(msg == null || msg.getResult() == null ? null : fromMedipostResultMapper.map(msg));
  }

  public Response sendResultMessage(ResultMessage message) {
    return medipostClient.sendResult(toMedipostResultMapper.map(message), message.getRecipient());
  }

  public Optional<AckMessage> getAckMessage(String messageId) {
    NotificationMessage msg = medipostClient.getNotification(messageId);
    return Optional.ofNullable(msg == null || msg.getNotification() == null ? null : fromMedipostAckMapper.map(msg));
  }

  public Response sendAckMessage(AckMessage message) {
    return medipostClient.sendNotification(toMedipostAckMapper.map(message), message.getRecipient());
  }

  public Response deletePrivateMessage(String messageId) {
    return medipostClient.deletePrivateMessage(messageId);
  }

  public List<MessageListItem> getListOfServiceListMessages() {
    return fromMedipostMessageListMapper.map(medipostClient.getPublicServicesMessageList());
  }

  public Optional<ServiceListMessage> getServiceListById(String messageId) {
    return Optional.ofNullable(medipostClient.getServiceList(messageId)).map(fromMedipostServiceListMapper::map);
  }

  public Optional<ServiceListMessage> getLatestServiceList(String sender) {
    return medipostClient.getLatestServiceList(sender).map(fromMedipostServiceListMapper::map);
  }

  public Response sendServiceList(ServiceListMessage message) {
    return medipostClient.sendServiceList(toMedipostServiceListMapper.map(message));
  }

  public Response deletePublicMessage(String messageId) {
    return medipostClient.deletePublicMessage(messageId);
  }
}